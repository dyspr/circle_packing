var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

var circles = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(128 + 128 * sin(frameCount * 0.05 + Math.random() * 0.5))
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < circles.length; i++) {
    var c = circles[i]
    c.show()

    if (c.growing) {
      c.grow()
      for (var j = 0; j < circles.length; j++) {
        var other = circles[j]
        if (other != c) {
          var d = dist(c.x, c.y, other.x, other.y)
          if (d - 1 < c.r + other.r) {
            c.growing = false
          }
        }
      }
      if (c.growing) {
        c.growing = !c.edges()
      }
    }
  }

  var target = 2
  var count = 0

  for (var i = 0; i < 100; i++) {
    if (addCircle()) {
      count++
    }
    if (count == target) {
      break
    }
  }

  if (count < 1) {
    circles = []
    console.log('finished')
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function addCircle() {
  var randDist = boardSize * 0.4 * Math.random()
  var randDir = Math.PI * 2 * Math.random()
  var newCircle = new Circle(randDist * sin(randDir), randDist * cos(randDir), 0)

  for (var i = 0; i < circles.length; i++) {
    var other = circles[i]
    var d = dist(newCircle.x, newCircle.y, other.x, other.y)
    if (d < other.r + 4) {
      newCircle = undefined
      break
    }
  }

  if (newCircle) {
    circles.push(newCircle)
    return true
  } else {
    return false
  }
}

function Circle(x, y, r) {
  this.growing = true
  this.x = x
  this.y = y
  this.r = r
}

Circle.prototype.edges = function() {
  return (dist(this.x + this.r, this.y + this.r, 0, 0) > boardSize * 0.45)
}

Circle.prototype.grow = function() {
  this.r += 1
}

Circle.prototype.show = function() {
  fill(128 + 128 * sin((0.1 + 0.05 * sin(frameCount * 0.05)) * dist(this.x, this.y, 0, 0) + Math.random() * 0.1))
  noStroke()
  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  ellipse(this.x, this.y, this.r * 2)
  pop()
}
